# Ad Module for WordPress Dashboard

This module is not to be used standalone. Integrate it in your theme or plugin. Integration 
procedure is given below.

The module reads the `src/assets/ads.json` file, schedules & displays the ads 
present in that file between dates mentioned for respective ad.

## How it Works

Let's say your ads.json file has this ad:
```json
[
    .
    .
    .
    {  
        "id":"awesome_ad_1",
        "icon":"ad-1-image.png",
        "title":"Ad 1 Title",
        "description":"Ad 1 Description Content",
        "start_date":"2018-12-11",
        "end_date":"2018-12-13",
        "link":"https://some-site-url.com/"
    }
    .
    .
    .
]

```
Ad module reads `ads.json` file only once until new changes are introduced in the ads.json file. If you 
make any changes in ads.json file, then make sure you create a hash of new file and add that hash in
`ad-hash.txt`. Otherwise module will not read your modified ads.json file. Reading all ads and scheduling
them is done only once until ads.json file is changed.

The hash of `ads.json` file can be calculated by firing below command in the terminal.
```bash 
md5sum ads.json
```

Coming back to the example, when it reads the above ad, it schedules a cron job that will be run on 11 Dec, 2018. When that cron
job is executed, it prepares the HTML of ad and saves it in the database as a transient named 'ad_html' with expiration time as
end date. In this example, expiration time will be set as 13 December, 2018. So this ad will be displayed between 11 December,2018
to 13 December,2018.

On every load/refresh, module checks if transient named 'ad_html' present or not and if that transient is present, then it 
displays the html associated with that tranisent.

If admin dismisses the ad, then that ad is not shown again in the dashboard but new ad will be shown when time comes!

**NOTE: The image passed to the icon attribute must be present `assets` directory.**

## How to Integrate

Follow the steps given below to integrate the module
1. All class files are required automatically using composer. Therefore Once you download this
package, run ```compose install```.
2. Do require_once `index.php` (available in this package) in your plugin/theme.

**NOTE: This module does not contain CSS/Styling for Ads. You can style them however you want!**

## Code Structure
```bash
.
├── composer.json - Composer Config file
├── index.php - Entry point for the Ad module. Require this file in your code
├── README.md - *YOU ARE READING THIS FILE
├── src
│   ├── assets
│   │   ├── ads-hash.txt - Contains hash of ads.json file
│   │   ├── ads.json - JSON of ads
│   │   └── promo-ads.js - JavaScript file required for the module. 
│   ├── includes
│   │   ├── class-ad-scheduler.php - Handles Scheduling of Ads
│   │   ├── class-promo-ads.php - Main Class file. index.php file creates object of this class. 
│   │   ├── class-transient-api.php - Custom Implementation of transients as WP transients are cleared by Caching plugins.
│   │   └── Entities
│   │       └── class-ad.php - Ad Entity Class. This validates the Ad object.
│   └── views
│       └── ad.php - Contains the HTML required for Ad file.
└── .gitignore - Files to be excluded from pushing into git repo

```
