<?php
use Promos\Promo_Ads as Promo_Ads;
use Promos\Ad_Scheduler as Ad_Scheduler;
use Promos\Transient_Api as Transient_Api;

require_once( 'vendor/autoload.php' );

Promo_Ads::get_instance();
