<?php
namespace Promos;

use Promos\Entities\Ad as Ad;

if ( ! class_exists( 'Promos\Ad_Scheduler' ) ) {
	/**
	 * Handles Scheduling of Ads and Callbacks of Scheduled Ads
	 *
	 * @package Promos
	 * @category Ad_Scheduler
	 * @since 1.0.0
	 */
	class Ad_Scheduler {

		private static $is_callback_registered = false;

		public function __construct() {
			$this->register_schedule_ad_callback();
		}

		public function register_schedule_ad_callback() {
			//Callback should be registered only once.
			if ( false === self::$is_callback_registered ) {
				add_action( 'schedule_ad', array( $this, 'scheduled_ad_callback' ), 10, 2 );
				self::$is_callback_registered = true;
			}
		}

		/**
		 * Callback to execute when Cron runs
		 *
		 * It gets the html required for ad to be displayed and stores it in the
		 * transient.
		 *
		 * @param Array $ad
		 * @param timestamp $expiration_time
		 */
		public function scheduled_ad_callback( $ad, $expiration_time ) {
			$ad = new Ad( $ad );
			if ( $ad ) {
				Transient_Api::set_transient( 'ad_html', $expiration_time, $ad->get_html() );
			}

		}

		/**
		 * Schedule when Ad should be displayed in the dashboard
		 *
		 * This function reads start and end date associated with the ad and schedules
		 * when the ad should be shown in the dashboard.
		 *
		 * Every ad should start displaying when day starts in New York and should
		 * stop displaying when day ends in Los Angeles
		 *
		 * @param Ad $ad
		 * @return void
		 */
		public function schedule( Ad $ad ) {
			$local_current_timestamp = current_time( 'timestamp' );
			$local_ad_start_timestamp = $this->get_ad_start_timestamp( $ad->start_date );
			$local_ad_end_timestamp = $this->get_ad_end_timestamp( $ad->end_date );

			//If Ad end time is already passed, then skip further processing
			if ( $local_current_timestamp >= $local_ad_end_timestamp ) {
				return;
			}

			$args = array(
				'ad' => (array) $ad,
				'expiration_time' => $this->get_ad_expiration_time( $local_current_timestamp, $local_ad_start_timestamp, $local_ad_end_timestamp ),
			);

			// NOTE: If ad start time is already passed, then WP will call the callback
			// immediately after scheduling the ad.
			wp_schedule_single_event( $local_ad_start_timestamp, 'schedule_ad', $args );
		}

		/**
		 * Returns timestamp of Ad Start Date Time
		 *
		 * @param string $start_date
		 * @return void
		 */
		private function get_ad_start_timestamp( $start_date ) {
			$start_date = new \DateTime( $start_date, new \DateTimeZone( 'America/New_York' ) );
			return $start_date->getTimestamp();
		}

		/**
		 * Returns timestamp of Ad End Date Time
		 *
		 * @param string $end_date
		 * @return void
		 */
		private function get_ad_end_timestamp( $end_date ) {
			$end_date = new \DateTime( $end_date . ' 23:59:59', new \DateTimeZone( 'America/Los_Angeles' ) );
			return $end_date->getTimestamp();
		}

		/**
		 * Returns Length of Ad Expiration
		 *
		 * @param timestamp $local_current_timestamp
		 * @param timestamp $local_ad_start_timestamp
		 * @param timestamp $local_ad_end_timestamp
		 * @return int  time in seconds
		 */
		private function get_ad_expiration_time( $local_current_timestamp, $local_ad_start_timestamp, $local_ad_end_timestamp ) {

			/// Ad start date is far from current time, then return full length of ad time
			if ( $local_current_timestamp < $local_ad_start_timestamp ) {
				return $local_ad_end_timestamp - $local_ad_start_timestamp;
			}

			// If above condition does not match, then that means Ad Start time is already passed.
			// Therefore ad should be shown for remaining time.
			return $local_ad_end_timestamp - $local_current_timestamp;
		}
	}
}
