<?php
namespace Promos;

use Promos\Entities\Ad as Ad;

if ( ! class_exists( 'Promos\Promo_Ads' ) ) {
	/**
	 * This class is responsibile for showing Ads in the dashboard
	 *
	 * There is `assets/ads.json` file which holds list of all ads. This
	 * class reads all those ads, schedule them for the display & display them when
	 * time comes.
	 *
	 * Sample content of ads.json file
	 * [
	 *       {
	 *          "id":"awesome_ad_1",
	 *            "icon":"ad_1_image.png",
	 *            "title":"Ad 1 Title",
	 *            "desc":"Ad 1 Description Content",
	 *            "start":"2018-12-11",
	 *            "end":"2018-12-13",
	 *            "link":"https://some-site-url.com/"
	 *        },
	 *        {
	 *            "id":"awesome_ad_2",
	 *            "icon":"ad_2_image.png",
	 *            "title":"Ad 2 Title",
	 *            "desc":"Ad 2 Description Content",
	 *            "start":"2018-12-26",
	 *            "end":"2019-01-03",
	 *            "link":"https://some-site-url.com/"
	 *       }
	 *  ]
	 *
	 * Every ad is shown between its respective Start Date and End Date.
	 *
	 * promo_ad folder contains one more file `ads-hash.txt` which contains hash
	 * of ads.json file
	 *
	 * @package Promos
	 * @category Promo_Ads
	 * @since 1.0.0
	 */
	class Promo_Ads {

		private $ad_scheduler = null;

		// Hold an instance of the class
		private static $instance = null;

		private function __construct( Ad_Scheduler $ad_scheduler ) {
			$this->ad_scheduler = $ad_scheduler;

			add_action( 'admin_init', array( $this, 'process_json_file' ) );
			add_action( 'admin_notices', array( $this, 'show_ad' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'load_custom_wp_admin_style_scripts' ) );
			add_action( 'wp_ajax_dismiss_notice_handler', array( $this, 'dismiss_ad' ) );
		}

		/**
		 * Method that returns current instance of the class. This class is made
		 * as a Singleton class because this module is going to be integrated in
		 * various plugin and if each of them creates a new object, then same
		 * function will be called more than once.
		 *
		 * Making this singleton will avoid recreation of other objects
		 *
		 * @return void
		 */
		public static function get_instance() {
			if ( null == self::$instance ) {
				self::$instance = new Promo_Ads( new Ad_Scheduler() );
			}
			return self::$instance;
		}

		/**
		 * Processes JSON File
		 *
		 * Processing JSON file involves reading ads and setting cron for each
		 * ad.
		 *
		 * Once all ads are scheduled, json file is not read until hash of the
		 * file changes.
		 *
		 */
		public function process_json_file() {
			if ( ! $this->is_json_file_updated() ) {
				return;
			}

			$this->read_ads_from_json_file();

			if ( empty( $this->ads ) ) {
				return;
			}

			foreach ( $this->ads as $ad ) {
				if ( ! empty( $ad['id'] ) ) {
					$this->ad_scheduler->schedule( new Ad( $ad ) );
				}
			}

			$this->json_read_complete();
		}

		/**
		 * Checks if `assets/ads.json` is updated/changed
		 *
		 * @return boolean
		 */
		private function is_json_file_updated() {
			$ad_json_file_hash = get_option( 'ad_json_file_hash', 0 );

			if ( $this->is_first_time_json_read( $ad_json_file_hash ) ) {
				return true;
			}

			if ( $this->is_hash_updated( $ad_json_file_hash ) ) {
				return true;
			}

			return false;
		}

		/**
		 * Reads the JSON File and creates a list of all ads mentioned in JSON File
		 */
		private function read_ads_from_json_file() {
			$ad_json_path = plugin_dir_path( __DIR__ ) . 'assets/ads.json';
			if ( file_exists( $ad_json_path ) ) {
				$str = file_get_contents( $ad_json_path );
				$this->ads = json_decode( $str, true );
			}
		}

		/**
		 * Save current JSON file's hash in the database
		 */
		private function json_read_complete() {
			$hash = file_get_contents( $this->get_hash_file_path() );
			if ( $hash ) {
				update_option( 'ad_json_file_hash', $hash );
			}
		}

		/**
		 * Checks if JSON file is being read for first time
		 *
		 * @param string $ad_json_file_hash Hash stored in the database
		 * @return boolean
		 */
		private function is_first_time_json_read( $ad_json_file_hash ) {
			return empty( $ad_json_file_hash );
		}

		/**
		 * Returns true if JSON file hash stored in the database does not match
		 * with the current JSON file hash.
		 *
		 * @param string $ad_json_file_hash Hash Stored in the database
		 * @return boolean
		 */
		private function is_hash_updated( $ad_json_file_hash ) {
			$hash_file_path = $this->get_hash_file_path();

			if ( ! file_exists( $hash_file_path ) ) {
				return false;
			}

			if ( file_get_contents( $hash_file_path ) != $ad_json_file_hash ) {
				return true;
			}

			return false;
		}

		/**
		 * Returns the path of Hash file.
		 *
		 * @return string
		 */
		private function get_hash_file_path() {
			return plugin_dir_path( __DIR__ ) . 'assets/ads-hash.txt';
		}

		/**
		 * Show Ad
		 *
		 * Ad content is stored in the transient. This function reads the transient
		 * and display the content.
		 *
		 * @return void
		 */
		public function show_ad() {
			if ( $ad_html = Transient_Api::get_transient( 'ad_html' ) ) {
				echo $ad_html;
			}
		}

		/**
		 * Enqueues Scripts required to display the Ad
		 */
		public function load_custom_wp_admin_style_scripts() {
			wp_register_script( 'promo-ads-js', plugins_url( 'assets/promo-ads.js', __DIR__ ), array( 'jquery' ), false, true );
			wp_enqueue_script( 'promo-ads-js' );
		}

		/**
		 * Stop Displaying Ad if user clicks dismissable button
		 *
		 * This is a Ajax callback when Dismiss Button is clicked
		 */
		public function dismiss_ad() {
			if ( ! empty( $_POST['nonce'] ) ) {
				Transient_Api::delete_transient( 'ad_html' );
			}
		}

	}
}
