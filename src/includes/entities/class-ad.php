<?php
namespace Promos\Entities;

if ( ! class_exists( 'Promos\Entities\Ad' ) ) {
	/**
	 * Class for Ad Entity
	 *
	 * @package Promos
	 * @category Ad
	 * @since 1.0.0
	 */
	class Ad {
		public $start_date;
		public $end_date;
		public $title;
		public $link;
		public $description;

		public function __construct( $ad ) {

			if ( empty( $ad['start_date'] ) ) {
				throw new \InvalidArgumentException( 'Start Date is required' );
			}

			if ( empty( $ad['end_date'] ) ) {
				throw new \InvalidArgumentException( 'End Date is required' );
			}

			if ( empty( $ad['title'] ) ) {
				throw new \InvalidArgumentException( 'Title is required' );
			}

			if ( empty( $ad['link'] ) ) {
				throw new \InvalidArgumentException( 'Link is required' );
			}

			if ( empty( $ad['description'] ) ) {
				throw new \InvalidArgumentException( 'Description is required' );
			}

			if ( empty( $ad['icon'] ) ) {
				throw new \InvalidArgumentException( 'Icon is required' );
			}

			$this->start_date = $ad['start_date'];
			$this->end_date = $ad['end_date'];
			$this->title = $ad['title'];
			$this->link = $ad['link'];
			$this->description = $ad['description'];
			$this->icon = $ad['icon'];
		}

		/**
		 * Prepares the HTML for Ad display
		 *
		 * @param Ad $ad
		 * @return string
		 */
		public function get_html() {
			$nonce = wp_create_nonce( 'ad-nonce' );
			$icon_url = plugins_url( 'assets/' . $this->icon, dirname( dirname( __FILE__ ) ) );
			ob_start();
			include plugin_dir_path( dirname( dirname( __FILE__ ) ) ) . 'views/ad.php';
			return ob_get_clean();
		}

	}

}
