<?php
namespace Promos;

use Promos\Entities\Ad as Ad;

if ( ! class_exists( 'Promos\Transient_Api' ) ) {
	/**
	 * Custom Implementation of Transients
	 *
	 * Implementing custom transients because caching plugins like W3TC, Super
	 * Cache deletes the WP default transients.
	 *
	 * @package Promos
	 * @category Transient_Api
	 *
	 */
	class Transient_Api {
		/**
		 * Set Transient
		 *
		 * @param string $transient Transient name.
		 * @param string $expiration Time until expiration in seconds from now, or 0 for never expires
		 * @param string $value Data to store in transient
		 */
		public static function set_transient( $transient, $expiration, $value = '' ) {
			if ( 0 == $expiration ) {
				$time_out = 0;
			} else {
				$time_out = strtotime( '+' . $expiration . ' day', current_time( 'timestamp' ) );
			}

			$data = array(
				'timeout' => $time_out,
				'value' => json_encode( $value ),
			);

			update_option( $transient, $data );
		}

		/**
		 * Get Transient
		 *
		 * @param string $transient Transient Name
		 */
		public static function get_transient( $transient ) {
			$cache = get_option( $transient );
			if ( 0 != $cache['timeout'] && ( empty( $cache['timeout'] ) || current_time( 'timestamp' ) > $cache['timeout'] ) ) {
				return false; // Cache is expired
			}

			return json_decode( $cache['value'] );
		}

		/**
		 * Delete Transient
		 *
		 * @param string $transient Transient Name
		 * @return void
		 */
		public static function delete_transient( $transient ) {
			delete_option( $transient );
		}
	}
}
