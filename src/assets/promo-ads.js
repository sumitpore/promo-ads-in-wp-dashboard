;(function( $ ) {
	"use strict";

	$( document ).ready(function() {
		$( '.ad-notice' ).on( 'click', '.notice-dismiss', function () {
			const nonce = $( this ).closest( '.ad-notice' ).data( 'nonce' );
			$.ajax( ajaxurl,
				{
					type: 'POST',
					data: {
						action: 'dismiss_notice_handler',
						nonce: nonce
					}
				} );
		});
	});

}(jQuery));
