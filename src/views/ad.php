<div class="notice is-dismissible ad-notice" data-nonce="<?php echo $nonce; ?>">
	<div class="ad-outerdiv">
		<p class="ad-icon"><img src="<?php echo $icon_url; ?>" /></p>
		<div class="ad-messagediv">
			<p class="ad-title"><?php echo $this->title; ?></p>
			<p class="ad-msg"><?php echo $this->description; ?></p>
		</div>
		<div class="actiondiv">
			<a target="_blank" class="button actionbtn" href="<?php echo $this->link; ?>">Shop Now</a>
		</div>
	</div>
</div>
